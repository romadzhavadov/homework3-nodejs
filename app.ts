import fs from 'fs';

// type Schema = Record<string, string | number | Date>;  // Record це вбудований тип даних у TypeScript, який представляє об'єкт, у якого ключі відповідають певному типу, а значення відповідають іншому типу.

type Schema = {
  id: number,
  title: string,
  text: string,
  createDate: Date,
};

interface Table<T extends Schema> {
  getAll(): T[];
  getById(id: number): T | undefined;
  create(item: Partial<T>): T;                    // Partial<T> - це вбудований тип у TypeScript, який створює новий тип, де всі властивості типу T стають необов'язковими (залишаються на рівні T | undefined).    
  update(id: number, updates: Partial<T>): T;
  delete(id: number): number;
}

interface FileDB {
  schemas: Record<string, Schema>;
  registerSchema<T extends Schema>(name: string, schema: T): void;
  getTable<T extends Schema>(name: string): Table<T>;
}

class FileDBImpl implements FileDB {
  schemas: Record<string, Schema> = {};

  registerSchema<T extends Schema>(name: string, schema: T): void {
    this.schemas[name] = schema;
  }

  getTable<T extends Schema>(name: string): Table<T> {
    const schema = this.schemas[name];
    if (!schema) {
      throw new Error(`Schema '${name}' is not registered.`);
    }

    return new TableImpl<T>(`${name}.json`, schema as T);
  }
}

class TableImpl<T extends Schema> implements Table<T> {
  constructor(private filename: string, private schema: T) {
    this.filename = filename;
    this.schema = schema;
  }

  getAll(): T[] {
    const data = this._readData();
    return data;
  }

  getById(id: number): T | undefined {
    const data = this._readData();
    return data.find((item: T) => (item as any).id === id);
  }

  create(item: T): T {
    const data = this._readData();
    const newItem = { ...item, id: Date.now() } as T;  
    data.push(newItem);
    this._writeData(data);
    return newItem;
  }

  update(id: number, updates: Partial<T>): T {
    const data = this._readData();
    const index = data.findIndex((item: T) => item.id === id);
    if (index === -1) {
      throw new Error(`Item with id ${id} not found.`);
    }

    const updatedItem = { ...data[index], ...updates } as T;
    data[index] = updatedItem;
    this._writeData(data);
    return updatedItem;
  }

  delete(id: number): number {
    const data = this._readData();
    const index = data.findIndex((item: T) => item.id === id);
    if (index === -1) {
      throw new Error(`Item with id ${id} not found.`);
    }

    data.splice(index, 1);
    this._writeData(data);
    return id;
  }

  private _readData(): T[] {
    try {
      const rawData = fs.readFileSync(this.filename, 'utf-8');
      return JSON.parse(rawData);
    } catch (error) {
      console.log(error)
      return [];
    }
  }

  private _writeData(data: T[]): void {
    fs.writeFileSync(this.filename, JSON.stringify(data, null, 2));
  }
}





const filename = 'FileDB'; 

const newspostSchema: Schema = {
  id: 1,
  title: 'Title',
  text: "Text",
  createDate: new Date(),
};

const db = new FileDBImpl();
db.registerSchema(filename, newspostSchema);

// Отримання схеми
const table = db.getTable(filename);
console.log('table', table);

// Створення нового запису
const newItem = table.create({ title: 'Item 1', text: 'item 1 text' });
console.log('New item created:', newItem);

// Отримання всіх записів
const allItems = table.getAll();
console.log('All items:', allItems);

// Отримання запису за ідентифікатором
const itemId = newItem.id;
const foundItem = table.getById(itemId);
console.log('Found item by id:', foundItem);

// Оновлення запису
const updatedItem = table.update(itemId, { title: 'Updated Item' });
console.log('Updated item:', updatedItem);

// Видалення запису
const deletedItemId = table.delete(itemId);
console.log('Deleted item id:', deletedItemId);


